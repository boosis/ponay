#!/usr/bin/env bash

nginx -v > /dev/null 2>&1
NGINX_IS_INSTALLED=$?

server_ip="$1"
host_ip=`echo $1 | sed 's/\.[0-9]*$/.1/'`
public_folder="$2"
host_name="$3"

sed -i "s/('127.0.0.1', 'fe80::1'/('127.0.0.1', '$host_ip', 'fe80::1'/" ${public_folder}/web/app_dev.php
sed -i "s/'127.0.0.1',$/'127.0.0.1', '$host_ip',/" ${public_folder}/web/config.php

function server_block {
    cat <<EOF
    server {
        server_name ${server_ip}.xip.io ${host_name};
        root ${public_folder}/web;

        location / {
            # try to serve file directly, fallback to app.php
            try_files \$uri /app.php\$is_args\$args;
        }
        # DEV
        # This rule should only be placed on your development environment
        # In production, don't include this and don't deploy app_dev.php or config.php
        location ~ ^/(app_dev|config)\.php(/|$) {
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            fastcgi_param HTTPS off;
        }
        # PROD
        location ~ ^/app\.php(/|$) {
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
            fastcgi_param HTTPS off;
            # Prevents URIs that include the front controller. This will 404:
            # http://domain.tld/app.php/some-path
            # Remove the internal directive to allow URIs like this
            internal;
        }

        access_log /var/log/nginx/vagrant.com-access.log;
        error_log  /var/log/nginx/vagrant.com-error.log error;

        location = /favicon.ico { log_not_found off; access_log off; }
        location = /robots.txt { log_not_found off; access_log off; }
    }

EOF

};

if [ ${NGINX_IS_INSTALLED} -eq 0 ]; then
    server_block > /etc/nginx/sites-available/vagrant
    service nginx reload
fi