<?php
namespace Deve\Google\Webmaster;

class Response
{
    protected $isError = false;
    protected $data = null;

    public function __construct($data = null)
    {
        $this->data = $data;
        if (!$this->data) {
            $this->isError = true;
        }
    }

    public function isError()
    {
        return $this->isError();
    }

    public function getData()
    {
        return $this->data;
    }
}