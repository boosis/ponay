<?php
namespace Deve\Google\Webmaster\Enum;

class Prop
{
    const VIDEO = 'VIDEO';
    const WEB = 'WEB';
    const MOBILE = 'MOBILE';
    const IMAGE = 'IMAGE';
    const ALL = 'ALL';
}