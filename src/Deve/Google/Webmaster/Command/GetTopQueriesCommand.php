<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class GetTopQueriesCommand extends Command
{
    // prop=WEB&region&db=20141021&de=20141119&more=true
    protected $path;
    protected $params;

    public function __construct($path, UrlParams $params)
    {
        $this->path = $path;
        $this->params = $params;
    }

    public function getPath()
    {
        return $this->path . '?' . $this->params->buildQuery();
    }
}