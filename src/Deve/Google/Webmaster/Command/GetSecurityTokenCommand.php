<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class GetSecurityTokenCommand extends Command
{
    protected $path = null;
    protected $params;

    public function __construct($path, UrlParams $params)
    {
        $this->path = $path;
        $this->params = $params;
    }


    public function getPath()
    {
        return $this->path . '?' . $this->params->buildQuery(true);
    }
}