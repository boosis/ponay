<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class GetContentKeywordsCommand extends Command
{
    protected $params;
    protected $path = 'content-words-dl';
    protected $tokenUri = 'keywords';

    public function __construct(UrlParams $params)
    {
        $this->params = $params;
    }

    public function getPath()
    {
        return $this->path . '?' . $this->params->buildQuery();
    }
}   