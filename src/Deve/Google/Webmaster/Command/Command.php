<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class Command
{
    protected $path = null;
    protected $tokenUri = null;
    protected $params;

    public function getPath()
    {
        return $this->path;
    }

    public function getTokenUri()
    {
        return $this->tokenUri;
    }

    public function requiresToken()
    {
        return (bool)$this->getTokenUri();
    }

    public function getRawPath()
    {
        return $this->path;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setSecurityCode($securityCode)
    {
        if (!$this->params) {
            $this->params = new UrlParams();
        }
        $this->params->setSecuirtyKey($securityCode);
    }
}