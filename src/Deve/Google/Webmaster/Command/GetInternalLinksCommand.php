<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class GetInternalLinksCommand extends Command
{
    protected $path = 'internal-links-dl';
    protected $params;
    protected $tokenUri = 'internal-links';

    public function __construct(UrlParams $urlParams)
    {
        $this->params = $urlParams;
    }

    public function getPath()
    {
        return $this->path . '?' . $this->params->buildQuery();
    }
}