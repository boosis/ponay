<?php
namespace Deve\Google\Webmaster\Command;

class GetTopPagesCommand extends Command
{
    // prop=WEB&region&db=20141021&de=20141119&more=true
    protected $path;
    protected $params;

    public function __construct($params)
    {
        $this->path = $params['path'];
        $params = $params['params'];
        $params['prob'] = isset($params['prob']) ? $params['prob'] : 'ALL';
        $params['db'] = isset($params['db']) ? $params['db'] : null;
        $params['de'] = isset($params['de']) ? $params['de'] : null;
        $params['more'] = isset($params['more']) ? $params['more'] : true;
        $params['qm'] = isset($params['qm']) ? $params['qm'] : null;
        $params['region'] = isset($params['region']) ? $params['region'] : null;
        $params['stars'] = isset($params['stars']) ? $params['stars'] : null;
        $params['qv'] = isset($params['qv']) ? $params['qv'] : null;
        $this->params = $params;
    }

    public function getPath()
    {
        return $this->path . '?' . http_build_query($this->params);
    }
}