<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class GetDownloadUrlsCommand extends Command
{
    protected $path = 'downloads-list';
    protected $params;

    public function __construct(UrlParams $urlParams)
    {
        $this->params = $urlParams;
    }

    public function getPath()
    {
        return $this->path . '?' . $this->params->buildQuery();
    }

}