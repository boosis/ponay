<?php
namespace Deve\Google\Webmaster\Command;

use Deve\Google\Webmaster\UrlParams;

class GetExternalLinksCommand extends Command
{
    protected $path = 'external-links-domain-dl';
    protected $params;
    protected $tokenUri = 'external-links-domain';

    public function __construct(UrlParams $urlParams)
    {
        $this->params = $urlParams;
    }

    public function getPath()
    {
        return $this->path . '?' . $this->params->buildQuery();
    }
}