<?php
namespace Deve\Google\Webmaster;

class UrlParams
{
    protected $hl = 'en';
    protected $prob = null;
    protected $db = null;
    protected $de = null;
    protected $more = true;
    protected $qm = null;
    protected $region = null;
    protected $stars = null;
    protected $qv = null;
    protected $siteUlr = null;
    protected $secuirtyKey = null;

    public function __construct($db = null, $de = null, $hl = 'en', $more = null, $prob = null, $qm = null, $qv = null, $region = null, $secuirtyKey = null, $siteUlr = null, $stars = null)
    {
        $this->db = $db;
        $this->de = $de;
        $this->hl = $hl;
        $this->more = $more;
        $this->prob = $prob;
        $this->qm = $qm;
        $this->qv = $qv;
        $this->region = $region;
        $this->secuirtyKey = $secuirtyKey;
        $this->siteUlr = $siteUlr;
        $this->stars = $stars;
    }

    public function getDb()
    {
        return $this->db;
    }

    public function setDb($db)
    {
        $this->db = $db;
    }

    public function getDe()
    {
        return $this->de;
    }

    public function setDe($de)
    {
        $this->de = $de;
    }

    public function getHl()
    {
        return $this->hl;
    }

    public function setHl($hl)
    {
        $this->hl = $hl;
    }

    public function getMore()
    {
        return $this->more;
    }

    public function setMore($more)
    {
        $this->more = $more;
    }

    public function getProb()
    {
        return $this->prob;
    }

    public function setProb($prob)
    {
        $this->prob = $prob;
    }

    public function getQm()
    {
        return $this->qm;
    }

    public function setQm($qm)
    {
        $this->qm = $qm;
    }

    public function getQv()
    {
        return $this->qv;
    }

    public function setQv($qv)
    {
        $this->qv = $qv;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;
    }

    public function getSecuirtyKey()
    {
        return $this->secuirtyKey;
    }

    public function setSecuirtyKey($secuirtyKey)
    {
        $this->secuirtyKey = $secuirtyKey;
    }

    public function getSiteUlr()
    {
        return $this->siteUlr;
    }

    public function setSiteUlr($siteUlr)
    {
        $this->siteUlr = $siteUlr;
    }

    public function getStars()
    {
        return $this->stars;
    }

    public function setStars($stars)
    {
        $this->stars = $stars;
    }

    public function buildQuery($forSecurityCode = false)
    {
        if ($forSecurityCode) {
            $urlParams = [
                'hl'      => '%s',
                'siteUrl' => '%s',
            ];

            return sprintf(str_replace('%25s', '%s', http_build_query($urlParams)), $this->getHl(), $this->getSiteUlr(), $this->getSecuirtyKey());
        }

        $urlParams = [
            'hl'             => '%s',
            'siteUrl'        => '%s',
            'security_token' => '%s',
            'prop'           => '%s',
            'db'             => '%s',
            'de'             => '%s',
            'more'           => '%s',
            'qm'             => '%s',
            'region'         => '%s',
            'stars'          => '%s',
            'qv'             => '%s',
        ];

        return sprintf(str_replace('%25s', '%s', http_build_query($urlParams)), $this->getHl(), $this->getSiteUlr(), $this->getSecuirtyKey(), $this->getProb(), $this->getDb(), $this->getDe(), $this->getMore(), $this->getQm(), $this->getRegion(), $this->getStars(), $this->getQv());
    }

}