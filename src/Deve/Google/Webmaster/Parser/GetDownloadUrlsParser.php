<?php
namespace Deve\Google\Webmaster\Parser;

class GetDownloadUrlsParser extends Parser
{

    function parse($data)
    {
        $urls = [];
        $decoded = json_decode($data, true);
        foreach ($decoded as $key => $value) {
            $value = str_replace('/webmasters/tools', '', $value);
            $valueBits = explode('?', $value);
            $urls[$key]['path'] = $valueBits[0];
            $params = explode('&', $valueBits[1]);
            foreach ($params as $param) {
                list($paramName, $paramValue) = explode('=', $param);
                $urls[$key]['params'][$paramName] = $paramValue;
            }

        }

        return $urls;
    }
}