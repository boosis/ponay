<?php
namespace Deve\Google\Webmaster\Parser;

use DOMDocument;

class GetSitesParser extends Parser
{

    function parse($data)
    {
        $sites = [];
        $doc = new DOMDocument();
        $doc->loadXML($data);
        /** @var \DOMNodeList $nodes */
        $nodes = $doc->getElementsByTagName('entry');
        foreach ($nodes as $node) {
            array_push($sites, $node->getElementsByTagName('title')->item(0)->nodeValue);
        }

        return $sites;
    }
}