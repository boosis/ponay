<?php
namespace Deve\Google\Webmaster\Parser;

class GetSecurityTokenParser extends Parser
{
    protected $delimiter;
    protected $dlUri = '';

    public function __construct($delimiter, $dlUri = '')
    {
        $this->delimiter = $delimiter;
        $this->dlUri = $dlUri;
    }

    public function parse($data)
    {
        $matches = [];
        preg_match_all("#{$this->dlUri}.*?46security_token(.*?){$this->delimiter}#si", $data, $matches);

        return isset($matches[1][0]) ? substr($matches[1][0], 3, -1) : '';
    }
}