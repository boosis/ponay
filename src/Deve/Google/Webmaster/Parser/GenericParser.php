<?php
namespace Deve\Google\Webmaster\Parser;

class GenericParser extends Parser
{

    function parse($data)
    {
        $parsed = [];
        $bits = explode("\n", $data);
        for ($i = 1; $i < count($bits); $i++) {
            $parsed[] = explode(',', $bits[$i]);
        }

        return $parsed;
    }
}