<?php
namespace Deve\Google\Webmaster\Parser;

abstract class Parser
{
    abstract function parse($data);
}