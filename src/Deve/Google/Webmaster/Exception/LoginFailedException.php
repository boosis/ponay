<?php
namespace Deve\Google\Webmaster\Exception;

class LoginFailedException extends \Exception
{
    protected $message = 'Login failed';
}