<?php
namespace Deve\Google\Webmaster\Exception;

class CouldNotAcquireSecurityTokenException extends \Exception
{
    protected $message = 'Could not acquire security token';
}