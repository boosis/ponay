<?php
namespace Deve\Google\Webmaster\Exception;

class NotLoggedInException extends \Exception
{
    protected $message = 'Not logged in.';
}