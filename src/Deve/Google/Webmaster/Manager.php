<?php
namespace Deve\Google\Webmaster;

use Deve\Google\Webmaster\Command\Command;
use Deve\Google\Webmaster\Command\GetSecurityTokenCommand;
use Deve\Google\Webmaster\Exception\CouldNotAcquireSecurityTokenException;
use Deve\Google\Webmaster\Parser\GetSecurityTokenParser;
use Deve\Google\Webmaster\Parser\Parser;

class Manager
{
    /** @var Connection */
    protected $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function executeCommand(Command $command, Parser $parser)
    {
        $this->connection->login();
        if ($command->requiresToken()) {
            $this->acquireToken($command);
        }
        $response = $this->connection->getData($command->getPath());

        $result = $parser->parse($response->getData());

        return $result;
    }

    private function acquireToken(Command $command)
    {
        /** @var UrlParams $urlParams */
        $urlParams = $command->getParams();
        $tokenCommand = new GetSecurityTokenCommand($command->getTokenUri(), $urlParams);
        $tokenParser = new GetSecurityTokenParser('\)', $command->getRawPath());
        $securityCode = $this->executeCommand($tokenCommand, $tokenParser);
        if (!$securityCode) {
            throw new CouldNotAcquireSecurityTokenException();
        }
        $command->setSecurityCode($securityCode);
    }
}