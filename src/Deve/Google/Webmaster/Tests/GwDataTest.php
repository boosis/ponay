<?php
namespace Deve\Google\Webmaster\Tests;
include __DIR__ . '/../gwdata.php';

class GwDataTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_test()
    {
        try {
            $email = "mehmet.korukmez@mediacom.com";
            $passwd = "WinterIsComing$0$";
            # If hardcoded, don't forget trailing slash!
            $website = "http://www.petplan.co.uk/";
            # Valid values are "TOP_PAGES", "TOP_QUERIES", "CRAWL_ERRORS",
            # "CONTENT_ERRORS", "CONTENT_KEYWORDS", "INTERNAL_LINKS",
            # "EXTERNAL_LINKS" and "SOCIAL_ACTIVITY".
            $tables = ["CRAWL_ERRORS"];
            $gdata = new \GWTdata();

            if ($gdata->LogIn($email, $passwd) === true) {
                $gdata->DownloadCSV($website, "./");
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

}