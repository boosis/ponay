<?php
namespace Deve\Google\Webmaster\Tests;

use Deve\Google\Webmaster\Command\GetContentKeywordsCommand;
use Deve\Google\Webmaster\Command\GetDownloadUrlsCommand;
use Deve\Google\Webmaster\Command\GetExternalLinksCommand;
use Deve\Google\Webmaster\Command\GetInternalLinksCommand;
use Deve\Google\Webmaster\Command\GetLatestBackLinksCommand;
use Deve\Google\Webmaster\Command\GetSecurityTokenCommand;
use Deve\Google\Webmaster\Command\GetSitesCommand;
use Deve\Google\Webmaster\Command\GetTopQueriesCommand;
use Deve\Google\Webmaster\Connection;
use Deve\Google\Webmaster\Manager;
use Deve\Google\Webmaster\Parser\GenericParser;
use Deve\Google\Webmaster\Parser\GetDownloadUrlsParser;
use Deve\Google\Webmaster\Parser\GetSecurityTokenParser;
use Deve\Google\Webmaster\Parser\GetSitesParser;
use Deve\Google\Webmaster\UrlParams;

class ConnectionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_login()
    {
        $connection = new Connection('bill@boosis.com', '39526282');
        $this->assertTrue($connection->login());
    }

    /**
     * @test
     */
    public function can_get_sites()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $command = new GetSitesCommand();
        $parser = new GetSitesParser();
        $data = $manager->executeCommand($command, $parser);
    }

    /**
     * @test
     */
    public function can_get_download_urls()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $params = new UrlParams();
        $params->setSiteUlr('http://www.petplan.co.uk/');
        $params->setHl('en');
        $command = new GetDownloadUrlsCommand($params);
        $parser = new GetDownloadUrlsParser();
        $data = $manager->executeCommand($command, $parser);
    }

    /**
     * @test
     */
    public function can_get_security_token()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $urlParams = new UrlParams();
        $urlParams->setSiteUlr('http://www.petplan.co.uk/');
        $urlParams->setHl('en');

        $command2 = new GetSecurityTokenCommand('keywords', $urlParams);
        $parser2 = new GetSecurityTokenParser('\)');
        $token = $manager->executeCommand($command2, $parser2);
    }

    /**
     * @test
     */
    public function can_get_top_pages()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);

        $urlParams = new UrlParams();
        $urlParams->setSiteUlr('http://www.petplan.co.uk/');
        $urlParams->setHl('en');
        $command = new GetDownloadUrlsCommand($urlParams);
        $parser = new GetDownloadUrlsParser();
        $urls = $manager->executeCommand($command, $parser);

        $urlParams->setSecuirtyKey($urls['TOP_QUERIES']['params']['security_token']);
        $urlParams->setDb('20141020');
        $urlParams->setDe('20141020');
        $command = new GetTopQueriesCommand($urls['TOP_QUERIES']['path'], $urlParams);
        $result = $manager->executeCommand($command, new GenericParser());
    }

    /**
     * @test
     */
    public function can_get_content_keywords()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $urlParams = new UrlParams();
        $urlParams->setSiteUlr('http://www.petplan.co.uk/');
        $urlParams->setHl('en');

        $command = new GetContentKeywordsCommand($urlParams);
        $parser = new GenericParser();

        $result = $manager->executeCommand($command, $parser);
    }

    /**
     * @test
     */
    public function can_get_external_links()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $urlParams = new UrlParams();
        $urlParams->setSiteUlr('http://www.petplan.co.uk/');
        $urlParams->setHl('en');

        $command = new GetExternalLinksCommand($urlParams);
        $parser = new GenericParser();

        $result = $manager->executeCommand($command, $parser);
    }

    /**
     * @test
     */
    public function can_get_internal_links()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $urlParams = new UrlParams();
        $urlParams->setSiteUlr('http://www.petplan.co.uk/');
        $urlParams->setHl('en');

        $command = new GetInternalLinksCommand($urlParams);
        $parser = new GenericParser();

        $result = $manager->executeCommand($command, $parser);
    }

    /**
     * @test
     */
    public function can_get_back_links()
    {
        $connection = new Connection('mehmet.korukmez@mediacom.com', 'WinterIsComing$0$');
        $manager = new Manager($connection);
        $urlParams = new UrlParams();
        $urlParams->setSiteUlr('http://www.petplan.co.uk/');
        $urlParams->setHl('en');

        $command = new GetLatestBackLinksCommand($urlParams);
        $parser = new GenericParser();

        $result = $manager->executeCommand($command, $parser);
    }
}