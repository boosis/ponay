<?php
namespace Deve\Google\Webmaster;

use Deve\Google\Webmaster\Exception\LoginFailedException;
use Deve\Google\Webmaster\Exception\NotLoggedInException;

class Connection
{
    const HOST = "https://www.google.com";
    const SERVICEURI = "/webmasters/tools/";
    protected $loggedIn = false;
    protected $token = null;
    protected $email = null;
    protected $password = null;

    function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function login()
    {
        if ($this->isLoggedIn()) {
            return true;
        } else {
            $url = self::HOST . "/accounts/ClientLogin";
            $postRequest = [
                'accountType' => 'HOSTED_OR_GOOGLE',
                'Email'       => $this->email,
                'Passwd'      => $this->password,
                'service'     => "sitemaps",
                'source' => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
            ];

            if ('@' === (string)$this->password[0] || version_compare(PHP_VERSION, '5.2.0') < 0) {
                $postRequest = http_build_query($postRequest);
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postRequest);
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if ($info['http_code'] == 200) {
                preg_match('/Auth=(.*)/', $output, $match);
                if (isset($match[1])) {
                    $this->token = $match[1];
                    $this->loggedIn = true;

                    return true;
                } else {
                    throw new LoginFailedException();
                }
            } else {
                throw new LoginFailedException();
            }
        }
    }

    public function isLoggedIn()
    {
        return $this->loggedIn;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getData($url)
    {
        if (!$this->isLoggedIn()) {
            throw new NotLoggedInException();
        }
        $url = self::HOST . self::SERVICEURI . ltrim($url, '/');
        $head = [
            "Authorization: GoogleLogin auth=" . $this->token,
            "GData-Version: 2"
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return new Response(($info['http_code'] != 200) ? false : $result);
    }

}