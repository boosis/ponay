<?php
namespace Ponay\Domain\User;

class Entity
{
    /** @var  Id */
    protected $id;
    /** @var  Email */
    protected $email;
    /** @var  Password */
    protected $password;
    /** @var  FullName */
    protected $fullName;

    private function __construct(Id $id)
    {
        $this->id = $id;
    }

    static public function factory(Id $id)
    {
        $entity = new static($id);

        return $entity;
    }

    public function getId()
    {
        return $this->id;
    }
}