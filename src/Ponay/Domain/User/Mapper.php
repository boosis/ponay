<?php
namespace Ponay\Domain\User;

class Mapper
{
    public function map(Entity $user)
    {
        $data = [];
        $id = $user->getId();
        if ($id->getValue()) {
            $data['_id'] = $id->getValue();
        }

        return $data;
    }

    public function hydrate($data)
    {
        $id = new Id($data['_id']);
        $post = Entity::factory($id);

        return $post;
    }
}