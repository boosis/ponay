<?php
namespace Ponay\Domain\Post;

class Tag
{
    protected $content;

    public function __construct($tag)
    {
        $this->content = $tag;
    }

    public function __toString()
    {
        return (string)$this->content;
    }
}