<?php
namespace Ponay\Domain\Post;

class Entity
{
    /** @var  Id */
    protected $id;
    /** @var  Author */
    protected $author;
    /** @var  Title */
    protected $title;
    /** @var  Description */
    protected $description;
    protected $tags;

    private function __construct(Id $id, Author $author, Title $title, Description $description, $tags = [])
    {
        $this->id = $id;
        $this->author = $author;
        $this->title = $title;
        $this->description = $description;
        $this->tags = $tags;
    }

    static public function factory(Id $id, Author $author, Title $title, Description $description, $tags = [])
    {
        $entity = new static($id, $author, $title, $description, $tags);

        return $entity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function geTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getTags()
    {
        return $this->tags;
    }
}