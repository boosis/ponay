<?php
namespace Ponay\Domain\Post;

class Title
{
    protected $content;

    public function __construct($title)
    {
        $this->content = $title;
    }

    public function __toString()
    {
        return (string)$this->content;
    }
}