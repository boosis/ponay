<?php
namespace Ponay\Domain\Post\Tests;

use Ponay\Domain\Post\Author;
use Ponay\Domain\Post\Description;
use Ponay\Domain\Post\Entity;
use Ponay\Domain\Post\Id;
use Ponay\Domain\Post\Mapper;
use Ponay\Domain\Post\Tag;
use Ponay\Domain\Post\Title;

class MapperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_map()
    {
        $title = new Title('title');
        $desc = new Description('desc');
        $author = new Author(1);
        $tags = [new Tag('tag')];
        $post = Entity::factory(new Id(), $author, $title, $desc, $tags);
        $mapper = new Mapper();
        $row = $mapper->map($post);
        $this->assertArrayNotHasKey('id', $row);
        $this->assertArrayHasKey('title', $row);
        $this->assertEquals('title', $row['title']);
    }

    /**
     * @test
     */
    public function can_hydrate()
    {
        $data = [
            '_id'         => 1,
            'title'       => 'title',
            'description' => 'desc',
            'aid'         => 1,
            'tags'        => [
                'tag'
            ]
        ];

        $mapper = new Mapper();
        /** @var Entity $post */
        $post = $mapper->hydrate($data);
        $this->assertEquals(1, $post->getId()->getValue());
        $this->assertCount(1, $post->getTags());
    }


}