<?php
namespace Ponay\Domain\Post\Tests;

use Ponay\Domain\Post\Title;

class TitleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_construct()
    {
        $title = new Title('Title value');
        $this->assertEquals('Title value', (string)$title);
    }

}