<?php
namespace Ponay\Domain\Post\Tests;

use Ponay\Domain\Post\Description;

class DescriptionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_construct()
    {
        $desc = new Description('Description value');
        $this->assertEquals('Description value', (string)$desc);
    }

}