<?php
namespace Ponay\Domain\Post;

class Description
{
    protected $content;

    public function __construct($desc)
    {
        $this->content = $desc;
    }

    public function __toString()
    {
        return (string)$this->content;
    }
}