<?php
namespace Ponay\Domain\Post;

class Mapper
{
    public function map(Entity $post)
    {
        $data = [];
        $id = $post->getId();
        if ($id->getValue()) {
            $data['_id'] = $id->getValue();
        }
        $author = $post->getAuthor();
        $data['aid'] = $author->getId();
        $data['title'] = (string)$post->geTitle();
        $data['description'] = (string)$post->getDescription();
        if ($post->getTags() && count($post->getTags())) {
            foreach ($post->getTags() as $tag) {
                $data['tags'][] = (string)$tag;
            }
        }

        return $data;
    }

    public function hydrate($data)
    {
        $title = new Title($data['title']);
        $description = new Description($data['description']);
        $author = new Author($data['aid']);
        $tags = [];
        if (isset($data['tags']) && count($data['tags']) > 0) {
            foreach ($data['tags'] as $tag) {
                $tags[] = new Tag($tag);
            }
        }
        $id = new Id($data['_id']);
        $post = Entity::factory($id, $author, $title, $description, $tags);

        return $post;
    }
}