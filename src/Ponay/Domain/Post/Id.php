<?php
namespace Ponay\Domain\Post;

class Id
{
    protected $id = null;

    public function __construct($id = null)
    {
        $this->id = $id;
    }

    public function getValue()
    {
        if ($this->id) {
            return $this->id;
        }

        return false;
    }
}