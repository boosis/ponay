<?php
namespace Ponay\Domain\Post;

class Author
{
    protected $id;

    public function __construct($authorId)
    {
        $this->id = $authorId;
    }

    public function getId()
    {
        return $this->id;
    }
}