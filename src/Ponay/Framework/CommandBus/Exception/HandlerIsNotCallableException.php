<?php
namespace Ponay\Framework\CommandBus\Exception;

class HandlerIsNotCallableException extends \Exception
{
    protected $message = 'Handler is not callable.';
}