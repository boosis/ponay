<?php
namespace Ponay\Framework\CommandBus\Exception;

class InvalidResponseException extends \Exception
{
    protected $message = 'Command handler must return \Ponay\Framework\CommandBus\Response';
}