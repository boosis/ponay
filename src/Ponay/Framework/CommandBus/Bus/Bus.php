<?php
namespace Ponay\Framework\CommandBus\Bus;

use Closure;
use Ponay\Framework\CommandBus\Command\CommandInterface;
use Ponay\Framework\CommandBus\Exception\CommandHandlerNotFoundException;
use Ponay\Framework\CommandBus\Exception\HandlerIsNotCallableException;
use Ponay\Framework\CommandBus\Exception\HandlerNotFoundException;
use Ponay\Framework\CommandBus\Exception\InvalidResponseException;
use Ponay\Framework\CommandBus\Resolver\NullResolver;
use Ponay\Framework\CommandBus\Resolver\ResolverInterface;
use Ponay\Framework\CommandBus\Response;
use Ponay\Framework\EventDispatcher\Dispatcher\Dispatcher;
use Zend\Stdlib\CallbackHandler;

class Bus
{
    /** @var ResolverInterface */
    protected $resolver = null;
    /** @var  Dispatcher */
    protected $eventDispatcher;
    protected $handlers = [];

    private function __construct(Dispatcher $eventDispatcher)
    {
        $this->handlers = [];
        $this->eventDispatcher = $eventDispatcher;
    }

    public function setResolver(ResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    public function getResolver()
    {
        if (!$this->resolver) {
            $this->resolver = new NullResolver();
        }

        return $this->resolver;
    }

    static public function factory(Dispatcher $eventDispatcher, ResolverInterface $resolver = null)
    {
        $static = new static($eventDispatcher);

        if (!$resolver) {
            $resolver = new NullResolver();
        }
        $static->setResolver($resolver);

        return $static;
    }

    public function registerHandler($commandName, $handler)
    {
        if (!is_callable($handler)) {
            throw new HandlerIsNotCallableException();
        }
        $this->handlers[$this->normalizeCommandName($commandName)] = $handler;
    }

    public function execute(CommandInterface $command)
    {
        $handler = $this->resolveHandler($command);
        /** @var Response $response */
        $response = $handler->call([$command]);
        if (!$response instanceof Response) {
            throw new InvalidResponseException();
        }
        if ($this->eventDispatcher) {
            $eventsTriggered = $response->getTriggeredEvents();
            foreach ($eventsTriggered as $event) {
                $this->eventDispatcher->trigger($event);
            }
        }

        return $response->getPayload();
    }

    private function resolveHandler(CommandInterface $command)
    {
        $commandName = $this->normalizeCommandName(get_class($command));
        if (!isset($this->handlers[$commandName])) {
            $this->handlers[$commandName] = $this->getResolver()->resolve($command);
            if (!isset($this->handlers[$commandName]) || !$this->handlers[$commandName]) {
                throw new HandlerNotFoundException();
            }
        }

        $handler = $this->handlers[$commandName];

        if ($handler instanceof Closure) {
            return new CallbackHandler($handler);
        } else {
            if (is_callable($handler)) {
                return new CallbackHandler($handler);
            }
            if (!is_callable([$handler, 'handle'])) {
                throw new HandlerIsNotCallableException();
            }

            return new CallbackHandler([$handler, 'handle']);
        }
    }

    private function normalizeCommandName($name)
    {
        return strtolower(strtr($name, ['-' => '', '_' => '', ' ' => '', '\\' => '', '/' => '']));
    }
}