<?php
namespace Ponay\Framework\CommandBus\Tests\Bus;

use Ponay\Framework\CommandBus\Bus\Bus;
use Ponay\Framework\CommandBus\Command\Command;
use Ponay\Framework\CommandBus\Command\CommandInterface;
use Ponay\Framework\CommandBus\Response;
use Ponay\Framework\EventDispatcher\Dispatcher\Dispatcher;

class BusTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_create()
    {
        $bus = Bus::factory(new Dispatcher());
        $this->assertInstanceOf(Bus::class, $bus);
    }

    /**
     * @test
     */
    public function can_resolve_internally()
    {
        $command = $this->getMockBuilder(CommandInterface::class)->getMock();

        $bus = Bus::factory(new Dispatcher());
        $bus->registerHandler(get_class($command), function (CommandInterface $command) {
            return new Response('command_handler_response');
        });

        $result = $bus->execute($command);

        $this->assertEquals('command_handler_response', $result);
    }

    /**
     * @test
     */
    public function can_execute()
    {
        $command = new Command();
        $command->setParam('param1', 'value1');

        $bus = Bus::factory(new Dispatcher());
        $bus->registerHandler(get_class($command), function (Command $command) {
            return new Response($command->getParam('param1'));
        });

        $result = $bus->execute($command);

        $this->assertEquals($result, 'value1');
    }
}