<?php
namespace Ponay\Framework\CommandBus\Command;

class Command implements CommandInterface
{
    protected $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function setParam($key, $value)
    {
        $this->data[$key] = $value;
    }

    public function getParam($key, $default = null)
    {
        return isset($this->data[$key]) ? $this->data[$key] : $default;
    }


}