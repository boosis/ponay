<?php
namespace Ponay\Framework\CommandBus;

use Ponay\Framework\EventDispatcher\EventEmitter;

class Response
{
    use EventEmitter;
    protected $payload;

    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
    }

    public function getPayload()
    {
        return $this->payload;
    }
} 