<?php
namespace Ponay\Framework\CommandBus\Resolver;

use Ponay\Framework\CommandBus\Command\CommandInterface;

class NullResolver implements ResolverInterface
{

    public function resolve(CommandInterface $command)
    {
        return false;
    }
}