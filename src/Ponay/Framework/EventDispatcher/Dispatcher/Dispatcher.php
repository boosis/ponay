<?php
namespace Ponay\Framework\EventDispatcher\Dispatcher;

use Ponay\Framework\EventDispatcher\Dispatcher\Exception\InvalidEventNameException;
use Ponay\Framework\EventDispatcher\Dispatcher\Exception\InvalidListenerException;
use Ponay\Framework\EventDispatcher\Event\Event;
use Ponay\Framework\EventDispatcher\Listener\Listener;

class Dispatcher
{
    protected $listeners;

    public function __construct()
    {
        $this->listeners = [];
    }

    public function addListener(Listener $listener)
    {
        $this->attachListener($listener->getEventName(), $listener->getCallback(), $listener->getPriority());
    }

    public function attachListener($eventName, callable $listener, $priority = 100)
    {
        if (!is_callable($listener)) {
            throw new InvalidListenerException();
        }
        if (!$eventName) {
            throw new InvalidEventNameException($eventName);
        }
        if ($priority) {
            $priority = 100;
        }
        $listeners = isset($this->listeners[$eventName]) ? $this->listeners[$eventName] : new ListenerQueue();
        $listeners->insert($listener, $priority);
        $this->listeners[$eventName] = $listeners;
    }

    public function getListeners($eventName)
    {
        return isset($this->listeners[$eventName]) ? $this->listeners[$eventName] : new ListenerQueue();
    }

    public function trigger($event)
    {
        $responses = new ResponseCollection();
        if (is_string($event)) {
            $event = Event::factory($event);
        }
        $listeners = $this->getListeners($event->getName());
        $listeners = clone $listeners;
        foreach ($listeners as $listener) {
            if (is_callable($listener) && !$event->propagationIsStopped()) {
                $responses->push(call_user_func_array($listener, [$event]));
            }
        }

        return $responses;
    }
}