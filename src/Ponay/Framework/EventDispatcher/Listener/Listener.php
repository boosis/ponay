<?php
namespace Ponay\Framework\EventDispatcher\Listener;

abstract class Listener
{
    protected $eventName;
    protected $priority;

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    function __construct($eventName, $priority = 100)
    {
        $this->eventName = $eventName;
        $this->priority = $priority;
    }

    public function getEventName()
    {
        return $this->eventName;
    }

    abstract public function getCallback();
}