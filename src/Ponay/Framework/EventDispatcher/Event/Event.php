<?php
namespace Ponay\Framework\EventDispatcher\Event;

use Ponay\Framework\EventDispatcher\Event\Exception\ParameterDoesNotExistsException;

class Event
{
    protected $stopPropagation = false;
    protected $name;
    protected $params = [];

    private function __construct($name, $params = [])
    {
        $this->name = $name;
        $this->params = $params;
    }

    static public function factory($name, $params = [])
    {
        $static = new static($name, $params);

        return $static;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function hasParam($key)
    {
        return isset($this->params[$key]);
    }

    public function getParam($key)
    {
        if (!$this->hasParam($key)) {
            throw new ParameterDoesNotExistsException($key);
        }

        return $this->params[$key];
    }

    public function setParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    public function stopPropagation($flag = true)
    {
        $this->stopPropagation = (bool)$flag;
    }

    public function propagationIsStopped()
    {
        return $this->stopPropagation;
    }
}