<?php
namespace Ponay\Framework\EventDispatcher\Tests\Event;

use Ponay\Framework\EventDispatcher\Event\Event;
use Ponay\Framework\EventDispatcher\Event\Exception\ParameterDoesNotExistsException;

class EventTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_create_event()
    {
        $event = Event::factory('name');
        $this->assertInstanceOf(Event::class, $event);
    }

    /**
     * @test
     */
    public function can_get_name()
    {
        $event = Event::factory('event_name');
        $this->assertEquals('event_name', $event->getName());
    }

    /**
     * @test
     */
    public function can_get_all_params()
    {
        $params = [
            'param1' => 'value1',
            'param2' => 'value2',
        ];
        $event = Event::factory('event_name', $params);
        $this->assertEquals($params, $event->getParams());
    }

    /**
     * @test
     */
    public function can_get_param_by_key()
    {
        $params = [
            'param1' => 'value1',
            'param2' => 'value2',
        ];
        $event = Event::factory('event_name', $params);
        $this->assertEquals('value1', $event->getParam('param1'));
    }

    /**
     * @test
     */
    public function throws_exception_is_param_does_not_exists()
    {
        $this->setExpectedException(ParameterDoesNotExistsException::class, 'non_existing_param');
        $params = [
            'param1' => 'value1',
            'param2' => 'value2',
        ];
        $event = Event::factory('event_name', $params);
        $event->getParam('non_existing_param');
    }


}