<?php
namespace Ponay\Framework\EventDispatcher\Tests\Dispatcher;

use Ponay\Framework\EventDispatcher\Dispatcher\Dispatcher;
use Ponay\Framework\EventDispatcher\Event\Event;
use Ponay\Framework\EventDispatcher\Listener\Listener;

class DispatcherTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function can_create_class()
    {
        $dispatcher = new Dispatcher();
        $this->assertInstanceOf(Dispatcher::class, $dispatcher);
    }

    /**
     * @test
     */
    public function can_add_listener()
    {
        $dispatcher = new Dispatcher();
        $dispatcher->attachListener('event_name', function (Event $event) {
            return $event->getName();
        });
        $this->assertCount(1, $dispatcher->getListeners('event_name'));
    }

    /**
     * @test
     */
    public function can_trigger_event()
    {
        $dispatcher = new Dispatcher();
        $dispatcher->attachListener('event_name', function (Event $event) {
            return 'event_response';
        });
        $event = Event::factory('event_name', ['param' => 'value']);
        $responses = $dispatcher->trigger($event);
        $this->assertEquals('event_response', $responses->pop());
    }

    /**
     * @test
     */
    public function obey_the_priority()
    {
        $dispatcher = new Dispatcher();
        $dispatcher->attachListener('event_name', function (Event $event) {
            $event->setParam('foo', 100);
        }, 100);
        $dispatcher->attachListener('event_name', function (Event $event) {
            $event->setParam('foo', 200);
        }, 200);
        $event = Event::factory('event_name', ['param' => 'value']);
        $dispatcher->trigger($event);
        $this->assertEquals(200, $event->getParam('foo'));
    }

    /**
     * @test
     */
    public function can_stop_propagation()
    {
        $dispatcher = new Dispatcher();
        $dispatcher->attachListener('event_name', function (Event $event) {
            $event->stopPropagation(true);
        }, 100);
        $dispatcher->attachListener('event_name', function (Event $event) {
            $event->setParam('foo', 200);
        }, 200);
        $event = Event::factory('event_name', ['param' => 'value']);
        $dispatcher->trigger($event);
        $this->assertFalse($event->hasParam('foo'));
    }

    /**
     * @test
     */
    public function can_attach_builtin_listener()
    {
        $listener = $this->getMockBuilder(Listener::class)
            ->setConstructorArgs(['event_name'])
            ->getMock();
        $listener->method('getCallback')
            ->willReturn(function (Event $event) {
            });
        $listener->method('getEventName')
            ->willReturn('event_name');

        $dispatcher = new Dispatcher();
        $dispatcher->addListener($listener);
        $this->assertCount(1, $dispatcher->getListeners('event_name'));
    }


}