<?php
namespace Ponay\Framework\EventDispatcher;

use SplStack;

trait EventEmitter
{
    protected $eventsTriggered;

    public function getTriggeredEvents()
    {
        if (!$this->eventsTriggered) {
            $this->eventsTriggered = new SplStack();
        }

        return $this->eventsTriggered;
    }

    public function triggerEvent($event)
    {
        if (!is_array($event)) {
            $event = [$event];
        }
        foreach ($event as $e) {
            $this->getTriggeredEvents()->push($e);
        }
    }
}